package com.example.harita.pickslot;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class addSlot extends AppCompatActivity {
    EditText datetext;
    EditText fromtimetxt;
    EditText totimetxt;
    EditText addAppemailtxt;
    EditText pass;
    Button addAppBut, viewAppBut, viewAvailBut;
    CheckBox viewAvaCkb;
    // TimePicker Time;
    DataBaseHelper persondb1;
    Button AddSlot_b;
    Date dateAdd;
    SimpleDateFormat dtfrm;
    private int year, month, day, hur, min;
    String finalDate = new String();
    String fromTime = new String();
    String toTime = new String();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_slot);
        fromtimetxt = (EditText) findViewById(R.id.FromTIme);
        totimetxt = (EditText) findViewById(R.id.ToTime);
        datetext = (EditText) findViewById(R.id.Date);
        addAppBut = (Button) findViewById(R.id.addApp);
        viewAppBut = (Button) findViewById(R.id.viewbtn);
        viewAvailBut =(Button) findViewById(R.id.view_avail_button);
       // viewAvaCkb = (CheckBox) findViewById(R.id.vaCheckBox);
        persondb1 = new DataBaseHelper(this);
        viewAvail();
        addData();
        /*
        viewAvaCkb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {

                    Log.d("test", " ###########checked#####################");
                } else {
                    Log.d("test", " ###########unchecked#####################");
                }
            }
        });*/

        //   viewAvailability();

        fromtimetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == fromtimetxt) {
                    final Calendar frmCal = Calendar.getInstance();
                    final TimePickerDialog.OnTimeSetListener FromTime = new
                            TimePickerDialog.OnTimeSetListener() {

                                @TargetApi(Build.VERSION_CODES.M)
                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                    StringBuffer fromTym = new StringBuffer();
                                    String ss = hourOfDay + "";
                                    if(ss.length() == 1)
                                        ss = "0"+ss;
                                    frmCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                    frmCal.set(Calendar.MINUTE, minute);
                                    fromtimetxt.setText("" + ss  + ":" + minute);
                                    fromTym.append(hourOfDay);
                                    fromTym.append(":");
                                    fromTym.append(minute);
                                    fromTime = fromTym.toString();
                                    Log.d("test", "time ******************************************+"+fromTime);

                                }

                            };
                    fromtimetxt.setOnTouchListener(new View.OnTouchListener() {

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                new TimePickerDialog(addSlot.this, FromTime, frmCal
                                        .get(Calendar.HOUR_OF_DAY), frmCal.get(Calendar.MINUTE),
                                        true).show();
                            }
                            return true;
                        }
                    });
                }
            }
        });
        totimetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == totimetxt) {
                    final Calendar ToCal = Calendar.getInstance();

                    final TimePickerDialog.OnTimeSetListener ToTime = new
                            TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                    StringBuffer toTym = new StringBuffer();
                                    ToCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                    ToCal.set(Calendar.MINUTE, minute);
                                    totimetxt.setText("" + hourOfDay + ":" + minute);
                                    toTym.append(hourOfDay);
                                    toTym.append(":");
                                    toTym.append(minute);
                                    toTime = toTym.toString();
                                    Log.d("test", "time ******************************************+"+toTime);

                                }


                            };
                    totimetxt.setOnTouchListener(new View.OnTouchListener() {

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                new TimePickerDialog(addSlot.this, ToTime, ToCal
                                        .get(Calendar.HOUR_OF_DAY), ToCal.get(Calendar.MINUTE),
                                        true).show();
                            }
                            return true;
                        }
                    });
                }
            }
        });


        datetext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == datetext) {
                    final Calendar myCalendar = Calendar.getInstance();

                    final DatePickerDialog.OnDateSetListener date = new
                            DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                   // monthOfYear=monthOfYear+1;
                                  //  Toast.makeText(addSlot.this,monthOfYear + "", Toast.LENGTH_LONG).show();
                                    // TODO Auto-generated method stub
                                    StringBuffer dat = new StringBuffer();
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                    dat.append(dayOfMonth);
                                    dat.append("/");
                                    dat.append(monthOfYear+1);
                                    dat.append("/");
                                    dat.append(year);
                                    finalDate= dat.toString();
                                    Log.d("test", "time ******************************************+"+finalDate);
                                    updateLabel();
                                }

                                private void updateLabel() {

                                    String myFormat = "dd/MM/yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.GERMANY);

                                    datetext.setText(sdf.format(myCalendar.getTime()));

                                }


                            };
                    datetext.setOnTouchListener(new View.OnTouchListener() {

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                new DatePickerDialog(addSlot.this, date, myCalendar
                                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                            }
                            return false;
                        }
                    });


                }

            }

        });
    }

    public void addData() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.GERMANY);
        final AlertDialog alrt = new AlertDialog.Builder(this).create();
        addAppBut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Date from = new Date();
                Date to = new Date();
                StringBuffer frm = new StringBuffer();
                StringBuffer bis= new StringBuffer();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

                frm.append(finalDate);
                frm.append(" ");
                frm.append(fromTime);

                bis.append(finalDate);
                bis.append(" ");
                bis.append(toTime);
                Log.d("test", "fromTime ################################"+frm+bis);
                try {
                    from =dateFormat.parse(frm.toString());
                    to = dateFormat.parse(bis.toString());
                     Log.d("test", "From"+from);
                    Log.d("test", "To"+to);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                addAppemailtxt = (EditText) findViewById(R.id.email_address);

                assert addAppemailtxt != null;
                String email = addAppemailtxt.getText().toString();
                pass = (EditText) findViewById(R.id.password);
                assert pass != null;

                String pswd = pass.getText().toString();


                String storedPassword = persondb1.getSinlgeEntry(email);
                if (pswd.equals(storedPassword)) {
                    ArrayList<String> fromList = persondb1.retrieveSlotFrom();
                    ArrayList<String> toList = persondb1.retrieveSlotTo();
                    Date dbFromDate = new Date();
                    Date dbToDate = new Date();
                    int checkif = 0;
                    if(fromList!=null) {
                        for (int i = 0; i < fromList.size(); i++) {
                            Log.d("test", "To" + fromList.get(i));
                            Log.d("test", "To" + from);
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            try {
                                dbFromDate = dateFormat1.parse(fromList.get(i));
                                dbToDate = dateFormat1.parse(toList.get(i));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if (from.after(dbFromDate) && from.before(dbToDate) || to.after(dbFromDate) && to.before(dbToDate)) {
                                Toast.makeText(addSlot.this, "Slot is already Taken. Please choose another time.", Toast.LENGTH_LONG).show();
                                checkif++;
                            }

                        }
                    }
                    if(checkif==0) {
                        if (from.before(to)) {
                            boolean slot_update = persondb1.insertDateAppointment(email, pswd, from, to, from, null, null);
                            if (slot_update = true) {
                                Toast.makeText(addSlot.this, "Data Inserted", Toast.LENGTH_SHORT).show();

                                Intent Home = new Intent(addSlot.this, HomeScreen.class);
                                startActivity(Home);
                            } else
                                Toast.makeText(addSlot.this, "Data not Inserted", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(addSlot.this, "Please enter correct TO  time", Toast.LENGTH_SHORT).show();
                    }




                    // addSlot.this.finish();
                    //  if(slot_update = true) Toast.makeText(BlockSlotActivity.this, "inserted" +day , Toast.LENGTH_LONG).show();
                    // else Toast.makeText(BlockSlotActivity.this, "Failed to insert slot", Toast.LENGTH_LONG).show();
                    //}
                    //else Toast.makeText(BlockSlotActivity.this, "Failed to stor password ", Toast.LENGTH_LONG).show();
                    //}

                }
               EditText emailData = (EditText) findViewById(R.id.email_address);
                email = emailData.getText().toString();
                EditText password = (EditText) findViewById(R.id.password);
                pswd = password.getText().toString();
                //emailData.getText().equals(null)
                alrt.setTitle("Missing Value");
                alrt.setMessage("Please Enter all the values");
                if (email.isEmpty() || pswd.isEmpty()) {
                    alrt.show();

                } else {
                    storedPassword = persondb1.getSinlgeEntry(email);
                    if (pswd.equals(storedPassword)) {
                        Toast.makeText(addSlot.this, "Congrats: Login Successfull", Toast.LENGTH_LONG).show();
                        //Toast.makeText(LoginActivity.this, "Data Successfully Inserted", Toast.LENGTH_LONG).show();
                        // startActivity(main);
                    } else {
                        Toast.makeText(addSlot.this, "User Name or Password does not match", Toast.LENGTH_LONG).show();
                    }
                }
            }

        });

    }
    public void viewAvail() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.GERMANY);
        final AlertDialog alrt = new AlertDialog.Builder(this).create();
        viewAvailBut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Date from = new Date();
                Date to = new Date();
                StringBuffer frm = new StringBuffer();
                StringBuffer bis= new StringBuffer();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

                frm.append(finalDate);
                frm.append(" ");
                frm.append(fromTime);

                bis.append(finalDate);
                bis.append(" ");
                bis.append(toTime);
                Log.d("test", "fromTime ################################"+frm+bis);
                try {
                    from =dateFormat.parse(frm.toString());
                    to = dateFormat.parse(bis.toString());
                    Log.d("test", "From"+from);
                    Log.d("test", "To"+to);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                    ArrayList<String> fromList = persondb1.retrieveSlotFrom();
                    ArrayList<String> toList = persondb1.retrieveSlotTo();
                    Date dbFromDate = new Date();
                    Date dbToDate = new Date();
                    if(fromList!=null) {
                        for (int i = 0; i < fromList.size(); i++) {
                            Log.d("test", "To" + fromList.get(i));
                            Log.d("test", "To" + from);
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String fromd = dateFormat1.format(from);
                            String tod = dateFormat1.format(to);
                            Log.d("test", "To" + fromd +tod);
                            try {
                                dbFromDate = dateFormat1.parse(fromList.get(i));
                                dbToDate = dateFormat1.parse(toList.get(i));
                                from = dateFormat1.parse(fromd);
                                to = dateFormat1.parse(tod);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            Log.d("test", "To" + from +to);
                            if (from.after(dbFromDate) && from.before(dbToDate) || to.after(dbFromDate) && to.before(dbToDate)) {
                                Log.d("test", "To" + from + dbFromDate + dbToDate+to);
                                Toast.makeText(addSlot.this, "Slot is already Taken. Please choose another time.", Toast.LENGTH_SHORT).show();
                                break;
                            }else {
                                Toast.makeText(addSlot.this, "Slot is Available.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }else {
                        Toast.makeText(addSlot.this, "Slot is Available.", Toast.LENGTH_SHORT).show();
                    }
            }

        });

    }

    public void viewAll() {
        viewAppBut.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor res = persondb1.getAllData();
                        if (res.getCount() == 0) {
                            Toast.makeText(addSlot.this, "No booking found", Toast.LENGTH_LONG).show();
                            return;

                        }
                        StringBuffer buffer = new StringBuffer();
                        while (res.moveToNext()) {
                            buffer.append("Id :" + res.getString(0) + "\n");
                            buffer.append("Email :" + res.getString(1) + "\n");
                            buffer.append("User :" + res.getString(2) + "\n");
                            buffer.append("From :" + res.getString(3) + "\n");
                            buffer.append("TO :" + res.getString(4) + "\n");
                            buffer.append("Status :" + res.getString(5) + "\\n");
                        }
                        Toast.makeText(addSlot.this, "Data " + buffer.toString(), Toast.LENGTH_LONG).show();
                        //showMessage("Data", buffer.toString());


                    }
                });


    }





   }
