package com.example.harita.pickslot;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.app.AlertDialog;
import android.widget.Toast;

import java.security.KeyStore;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FirstLayoutActivity extends AppCompatActivity {
    EditText editText, emailData, fName, lName, password;
    Button registerButton;
    String[] email;
    String subject;
    String content;
    DataBaseHelper persondb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        editText = (EditText)findViewById(R.id.Fname_edit_text);
        if(editText.requestFocus()) {
          InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            Snackbar.make(view, "Send an Email", Snackbar.LENGTH_LONG)
                  .setAction("Action", null).show();
        }
        });
       registerButton = (Button) this.findViewById(R.id.register);
        assert registerButton != null;

        persondb = new DataBaseHelper(this);
        addData();
    }

    public void addData() {
        final AlertDialog alrt = new AlertDialog.Builder(this).create();
        registerButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                emailData = (EditText)findViewById(R.id.email_address);
                String email = emailData.getText().toString();
                fName = (EditText) findViewById(R.id.Fname_edit_text);
                String fn = fName.getText().toString();
                lName = (EditText) findViewById(R.id.Lname_edit_text);
                String ln = lName.getText().toString();
                password =  (EditText) findViewById(R.id.password);
                String pswd = password.getText().toString();
                //emailData.getText().equals(null)
                alrt.setTitle("Missing Value");
                alrt.setMessage("Please Enter all the values");
                if(email.isEmpty()||fn.isEmpty()||ln.isEmpty()||pswd.isEmpty())
                {
                    alrt.show();

                }
                else {
                    boolean check = isEmailValid(email);
                    if (check == true) {
                        boolean isInserted = persondb.insertData(fn, ln, email, pswd);
                        if (isInserted == true) {
                            Intent Home = new Intent(FirstLayoutActivity.this, HomeScreen.class);
                            startActivity(Home);
                            Toast.makeText(FirstLayoutActivity.this, "Data Successfully Inserted", Toast.LENGTH_LONG).show();

                        }
                        else
                            Toast.makeText(FirstLayoutActivity.this, "Insertion Failed", Toast.LENGTH_LONG).show();

                    }
                    else
                        Toast.makeText(FirstLayoutActivity.this, "Email should contain @ and .", Toast.LENGTH_LONG).show();
                }
                }



        });
    }
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_first_layout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

