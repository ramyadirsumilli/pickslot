package com.example.harita.pickslot;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

public class enterPin extends AppCompatActivity {

    EditText pin;
    Button enterPin_b;
    DataBaseHelper persondb;
    ToggleButton ON;
    private ProgressDialog counterDia;
    private CounterHandler cntHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_pin2);
        counterDia = new ProgressDialog(this);
        counterDia.setTitle("10min Timer Starts ");
        counterDia.setMessage("Please wait  ");
        cntHandler=new CounterHandler();

        counterDia.setCancelable(true);

        //setContentView(R.layout.activity_home_screen);
        persondb = new DataBaseHelper(this);
        enterPin_b = (Button) this.findViewById(R.id.EnterPin);
        assert enterPin_b != null;
        ON = (ToggleButton) this.findViewById(R.id.ON);

        enterPin_b.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pin = (EditText) findViewById(R.id.PIN);
                String PIN = pin.getText().toString();
                String email = persondb.getEmailid(PIN);
                if(email.equals("NOT EXIST")) {
                    Intent Home = new Intent(enterPin.this, HomeScreen.class);
                    startActivity(Home);
                    Toast.makeText(enterPin.this, "PIN not Recognized", Toast.LENGTH_LONG).show();
                }
                else Toast.makeText(enterPin.this, "Congrats: Welcome "+email, Toast.LENGTH_LONG).show();
                counterDia.show();
                Thread thr=new Thread(new Counter());
                thr.start();


            }

        }
        ));


    }
    public class Counter implements Runnable
    {

        @Override
        public void run() {
            int i;
            for (i = 10; i >= 0; i--) {
                try {
                    Thread.sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Bundle bundle = new Bundle();
                bundle.putInt("Value", i);
                Message msg = new Message();
                msg.setData(bundle);
                cntHandler.sendMessage(msg);
            }
            counterDia.dismiss();
        }
    }
    private class CounterHandler extends Handler {

        public void handleMessage(Message mess){
            int valueTime=mess.getData().getInt("Value");
            counterDia.setMessage("Wait..00:"+valueTime);

        }


    }
}
