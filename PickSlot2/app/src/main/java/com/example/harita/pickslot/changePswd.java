package com.example.harita.pickslot;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class changePswd extends AppCompatActivity {

    EditText emailData, password, pswd1, pswd2;
    Button ConfirmPswd_b;
    DataBaseHelper persondb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pswd);
        ConfirmPswd_b = (Button) this.findViewById(R.id.pswdChanges_b);
        assert  ConfirmPswd_b!=null;
        persondb = new DataBaseHelper(this);
        enterPswd();
    }
    private void enterPswd() {
        final AlertDialog alrt = new AlertDialog.Builder(this).create();
        final AlertDialog alrt1 = new AlertDialog.Builder(this).create();
        ConfirmPswd_b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                emailData = (EditText) findViewById(R.id.email_address);
                String email = emailData.getText().toString();
                password = (EditText) findViewById(R.id.password);
                String pswd = password.getText().toString();
                pswd1 = (EditText) findViewById(R.id.enter_new_pswd);
                String NewPassword = pswd1.getText().toString();
                pswd2 = (EditText) findViewById(R.id.reenter_new_pswd);
                String NewPassword1 = pswd2.getText().toString();
                //emailData.getText().equals(null)
                alrt.setTitle("Missing Value");
                alrt.setMessage("Please Enter all the values");
                if (NewPassword.isEmpty() || NewPassword1.isEmpty()) {
                    alrt.show();

                } else {
                    String storedPassword = persondb.getSinlgeEntry(email);
                    if (NewPassword.equals(NewPassword1)){
                        if (pswd.equals(storedPassword)) {
                            boolean pswd_update = persondb.updatePswd(email,NewPassword);
                            if(pswd_update= true) Toast.makeText(changePswd.this, "Congrats: Password Successfully Updated", Toast.LENGTH_LONG).show();
                            else Toast.makeText(changePswd.this, "Failed to Change Password", Toast.LENGTH_LONG).show();
                            //Toast.makeText(LoginActivity.this, "Data Successfully Inserted", Toast.LENGTH_LONG).show();
                            // startActivity(main);
                        } else {
                            Toast.makeText(changePswd.this, "User Name or Password does not match", Toast.LENGTH_LONG).show();
                        }
                    }else{
                        alrt1.setTitle("Incoorect Password");
                        alrt1.setMessage("Please reenter the same new password");
                        alrt1.show();

                    }

                }
            }
        });

    }
}
