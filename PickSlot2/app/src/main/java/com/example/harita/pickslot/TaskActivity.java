package com.example.harita.pickslot;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.*;

public class TaskActivity extends AppCompatActivity {

    private static final String TAG = "TaskActivity";
    DataBaseHelper persondb;
    private ListView mTaskListView, mEmailListView;
    private ArrayAdapter<String> dAdapter;
    private ArrayAdapter<String> demAdapter;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        mTaskListView = (ListView) findViewById(R.id.list_task);
        mEmailListView= (ListView) findViewById(R.id.list_email);
        persondb = new DataBaseHelper(this);
        updateUI();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task, menu);
        updateUI();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_task:

                final EditText taskEditText = new EditText(this);
                final ArrayList<String> emails = persondb.getEmails();
                if(emails!=null) {
                    final String[] emailArray = emails.toArray(new String[emails.size()]);
                    final String[] emailSelected = {new String()};
                    final AlertDialog dialog = new AlertDialog.Builder(this)
                            .setTitle("Add a new Task")
                            .setMessage("Todo Task")
                            .setView(taskEditText)
                            .setNeutralButton("Assign To", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // dialogInterface.cancel();
                                    AlertDialog dialog1 = new AlertDialog.Builder(TaskActivity.this)
                                            .setTitle("Assign Task to: ")
                                            .setItems(emailArray, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    String task = String.valueOf(taskEditText.getText());
                                                    //  Log.d(TAG, "Add a new task"+task);
                                                    persondb.insertTask(task, emailArray[i]);
                                                    updateUI();
                                                }
                                            })
                                            .setNegativeButton("Cancel", null)
                                            .create();
                                    dialog1.show();
                                }
                            })

                            .setNegativeButton("Cancel", null)
                            .create();
                    dialog.show();
                    updateUI();
                }
                else
                    Toast.makeText(TaskActivity.this, "No Registered Emails ", Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void updateUI() {
        ArrayList<String> taskList = new ArrayList<>();
        taskList = persondb.retrieveTasks();
        ArrayList<String> emailList = persondb.retrieveEmails();

        if(taskList!=null&&emailList!=null) {
            if (demAdapter == null) {
                demAdapter = new ArrayAdapter<>(this, R.layout.task_todo, R.id.task_email, emailList);
                mEmailListView.setAdapter(demAdapter);

            } else {
                demAdapter.clear();
                demAdapter.addAll(emailList);
                demAdapter.notifyDataSetChanged();
            }

            if (dAdapter == null) {
                dAdapter = new ArrayAdapter<>(
                        this, R.layout.task_todo,
                        R.id.task_title,
                        taskList);
                mTaskListView.setAdapter(dAdapter);

            } else {
                dAdapter.clear();
                dAdapter.addAll(taskList);
                dAdapter.notifyDataSetChanged();
            }
        }
    }

    public void removeTask(View view) {
        View parent = (View) view.getParent();
        TextView taskTextView = (TextView) parent.findViewById(R.id.task_title);
        String task = String.valueOf(taskTextView.getText());
        persondb.deleteTask(task);
        updateUI();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Task Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.harita.pickslot/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Task Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.harita.pickslot/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
