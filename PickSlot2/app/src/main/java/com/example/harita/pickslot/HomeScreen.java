package com.example.harita.pickslot;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Arrays;

public class HomeScreen extends AppCompatActivity {
    ToggleButton ON;
    Button AdminSettings, UserSettings, Search, AddSlot;
    private ListView mFromListView,mToListView, mEmailListView;
    DataBaseHelper persondb;
    private ArrayAdapter<String> emAdapter;
    private ArrayAdapter<String> toAdapter;
    private ArrayAdapter <String>fromAdapter ;
    private static final String TAG="TaskActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        persondb = new DataBaseHelper(this);
        mFromListView = (ListView) findViewById(R.id.from_list);
       // mToListView =(ListView) findViewById(R.id.to_list);
       // mEmailListView=(ListView)findViewById(R.id.user_list);


        ON= (ToggleButton) this.findViewById(R.id.ON);
        AdminSettings = (Button) this.findViewById(R.id.AdminSettings);
        UserSettings = (Button) this.findViewById(R.id.UserSettings);
       // Search = (Button) this.findViewById(R.id.searchView);
        AddSlot = (Button) this.findViewById(R.id.AddUser);
        assert ON!= null;
        assert AdminSettings!= null;
        assert UserSettings!= null;
        //assert Search!= null;
        assert AddSlot!= null;
        /*ON.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked) {
                    Intent Pin = new Intent(HomeScreen.this,enterPin.class);
                    startActivity(Pin);
                    Toast.makeText(HomeScreen.this, "Congrats: Welcome ", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(HomeScreen.this, "Room is Available ", Toast.LENGTH_LONG).show();
                } 
            }
        });*/

        ON.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ON.isChecked()) {
                    ON.setBackgroundColor(Color.GREEN);
                    Intent Pin = new Intent(HomeScreen.this, enterPin.class);
                    startActivity(Pin);
                    Toast.makeText(HomeScreen.this, "Congrats: Welcome ", Toast.LENGTH_LONG).show();
                }else {
                    ON.setBackgroundColor(Color.RED);
                    Toast.makeText(HomeScreen.this, "Room is Available ", Toast.LENGTH_LONG).show();
                }
            }
        }
        ));
        AdminSettings.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent Admin = new Intent(HomeScreen.this,AdminActivity.class);
                startActivity(Admin);
                Toast.makeText(HomeScreen.this, "Welcome Admin", Toast.LENGTH_LONG).show();
            }
        }
        ));
        UserSettings.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent User = new Intent(HomeScreen.this,UserSettings.class);
                startActivity(User);
                Toast.makeText(HomeScreen.this, "Congrats: Welcome ", Toast.LENGTH_LONG).show();
            }
        }
        ));
       /* Search.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(HomeScreen.this, "To be developed", Toast.LENGTH_LONG).show();
            }
        }
        ));*/
        AddSlot.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent Add = new Intent(HomeScreen.this,addSlot.class);
                startActivity(Add);
                Toast.makeText(HomeScreen.this, "book slot ", Toast.LENGTH_LONG).show();
            }
        }
        ));

        updateUI();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void updateUI() {

        ArrayList<String> emailList = persondb.retrieveSlotEmails();
        ArrayList<String> fromList = persondb.retrieveSlotFrom();
        ArrayList<String> toList = persondb.retrieveSlotTo();
        if(emailList!=null&&fromList!=null && toList!=null){

        ArrayList<String> infprmation = new ArrayList<String>();
        int i = 0;
        for(String s : emailList){
            java.util.Date Date = new java.util.Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String d = sdf.format(Date);

            if(fromList.get(i).contains(d))
            {
                infprmation.add(fromList.get(i) + "    \t\t\t" + toList.get(i) + "    \t\t\t" + s );
                i++;
            }

        }


            if (fromAdapter == null) {

                fromAdapter= new ArrayAdapter<>(
                        this, R.layout.list_home,
                        R.id.from_text,
                        infprmation);
              mFromListView.setBackgroundColor(Color.parseColor("#BBDEFB"));
                 mFromListView.setAdapter(fromAdapter);

            } else {
                fromAdapter.clear();
                fromAdapter.addAll(infprmation);
                fromAdapter.notifyDataSetChanged();
            }
          /*  if (toAdapter == null) {
                toAdapter= new ArrayAdapter<>(
                        this, R.layout.list_home,
                        R.id.to_text,
                        toList);
                mToListView.setAdapter(toAdapter);

            } else {
                toAdapter.clear();
                toAdapter.addAll(emailList);
                toAdapter.notifyDataSetChanged();
            }
            if (emAdapter == null) {
                emAdapter= new ArrayAdapter<>(
                        this, R.layout.list_home,
                        R.id.user_text,
                        emailList);
                mEmailListView.setAdapter(emAdapter);

            } else {
                emAdapter.clear();
                emAdapter.addAll(emailList);
                emAdapter.notifyDataSetChanged();
            }*/
        }
    }
}
