package com.example.harita.pickslot;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import java.util.Date;
import java.security.KeyStore;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.*;

/**
 * Created by ramya on 5/23/16.
 */
public class DataBaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "person.db";
    public static final String PERSON_TABLE = "person_table";
    public static final String COL_1 = "id";
    public static final String COL_2 = "persontype";
    public static final String COL_3 = "firstname";
    public static final String COL_4 = "lastname";
    public static final String COL_5 = "emailid";
    public static final String COL_6 = "psw";
    public static final String COL_7 = "pin";
    public static final String SLOT_TABLE = "slot_table";
    public static final String SCOL_1 = "id";
    public static final String SCOL_2 = "emailid";
    public static final String SCOL_3 = "psw";
    public static final String SCOL_4 = "frmtime";
    public static final String SCOL_5 = "totime";
    public static final String SCOL_6 = "mdate";
    public static final String SCOL_7 = "room";
    public static final String SCOL_8 = "status";
    public static final String TASK_TABLE = "task_table";
    public static final String TCOL_1 = "id";
    public static final String TCOL_2 = "task";
    public static final String TCOL_3 = "emailid";
    ContentValues contentvalue =  new ContentValues();

    private static final String TAG="TaskActivity";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " +PERSON_TABLE +"(id INTEGER PRIMARY KEY AUTOINCREMENT, persontype TEXT, firstname TEXT, lastname TEXT, emailid TEXT, psw TEXT, pin INTEGER)");
        db.execSQL("create table " +SLOT_TABLE +"(ID INTEGER PRIMARY KEY AUTOINCREMENT,emailid TEXT, psw TEXT,frmtime datetime,totime datetime,mdate datetime,room TEXT,status TEXT)" );
        db.execSQL("create table " +TASK_TABLE +"(ID INTEGER PRIMARY KEY AUTOINCREMENT, task TEXT NOT NULL, emailid TEXT NOT NULL)" );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " +PERSON_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +SLOT_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +TASK_TABLE);
        onCreate(db);
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public DataBaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }
    public Cursor getInformation(SQLiteDatabase db) {
        String[] projections = {"COL_3","COL_5"};
        Cursor cur = db.query(PERSON_TABLE,projections,null,null,null,null,null);

        return  cur;

    }

    public boolean insertData(String firstname, String lastname, String emailid, String password){
        SQLiteDatabase db = this.getWritableDatabase();

        contentvalue.put(COL_3,firstname);
        contentvalue.put(COL_4,lastname);
        contentvalue.put(COL_5,emailid);
        contentvalue.put(COL_6, password);
        long result = db.insert(PERSON_TABLE,null,contentvalue);
        if (result == -1)return false;
        else return true;

    }

    public  boolean updatePin(String em, String pin){
        SQLiteDatabase db = this.getWritableDatabase();
        contentvalue.put(COL_7,pin);
        String emailcolumn = "emailid= ?";

        int result = db.update(PERSON_TABLE, contentvalue, emailcolumn, new String[]{em});
        if(result==-1)return false;
        else return true;
    }
    public  boolean updatePswd(String em, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        contentvalue.put(COL_6,password);
        String emailcolumn = "emailid= ?";

        int result = db.update(PERSON_TABLE, contentvalue, emailcolumn, new String[]{em});
        if(result==-1)return false;
        else return true;
    }
    public String getSinlgeEntry(String em) {
        SQLiteDatabase db = this.getReadableDatabase();
        String emailcolumn = "emailid= ?";

        //Cursor cursor = db.query(TABLE_NAME,new String[] {"password"},emailcolumn, new String[] {em}, null, null, null);
        Cursor cursor = db.query(PERSON_TABLE,null,emailcolumn, new String[] {em}, null, null, null);

        if (cursor.getCount() < 1)
       {
            cursor.close();
            return "NOT EXIST";
        }
        cursor.moveToFirst();
        String password = cursor.getString(cursor.getColumnIndex(COL_6));

        cursor.close();
        return password;
    }

    public String getEmailid(String PIN){
        SQLiteDatabase db = this.getReadableDatabase();
        String pincolumn = "pin= ?";
        Cursor cursor = db.query(PERSON_TABLE,null,pincolumn, new String[] {PIN}, null, null, null);
        if (cursor.getCount() < 1)
        {
            cursor.close();
            return "NOT EXIST";
        }
        cursor.moveToFirst();
        String email = cursor.getString(cursor.getColumnIndex(COL_5));

        cursor.close();
        return email;

    }

    public boolean insertSData(String emailid, String date, String time){
        SQLiteDatabase db = this.getWritableDatabase();

        contentvalue.put(SCOL_2,emailid);
        contentvalue.put(SCOL_3,date);
        contentvalue.put(SCOL_4,time);
        long result = db.insert(SLOT_TABLE,null,contentvalue);
        if (result == -1)return false;
        else return true;

    }



    public boolean insertDateAppointment (String email, String psw ,Date From,Date To,Date mdt, String Room, String Status) {
       SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SCOL_2, email);
        contentValues.put(SCOL_3, psw);
        contentValues.put(SCOL_4, getDateTime(From));
        contentValues.put(SCOL_5,getDateTime(To));
        contentValues.put(SCOL_6, getDateTime(mdt));
        contentValues.put(SCOL_7 , Room);
        contentValues.put(SCOL_8, Status);
        long result = db.insert(SLOT_TABLE, null, contentValues);
        if (result == 1)
            return false;
        else
            return true;
    }
   public void insertTask(String task, String email){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
       // contentValues.put(TCOL_2, email);
        contentValues.put(TCOL_2, task);
       contentValues.put(TCOL_3, email);
       Log.d(TAG, "Add a new task"+task +email);
        long result = db.insert(TASK_TABLE, null, contentValues);
    }
    public ArrayList<String> retrieveTasks(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> taskList= new ArrayList<>();
        String[] projections = {TCOL_2};
        Cursor cursor = db.query(TASK_TABLE,projections,null, null, null, null, null);
       /* if (cursor.getCount() < 1)
        {
            cursor.close();
            return "NOT EXIST";
        }*/
        if(cursor.moveToFirst()){


        do{
           int idx = cursor.getColumnIndex(TCOL_2);
            taskList.add(cursor.getString(idx));
        } while(cursor.moveToNext());
        }else{
            return null;
        }
        Log.d(TAG, "Task List"+taskList);
        cursor.close();
        return taskList;
    }
    public ArrayList<String> retrieveEmails(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> emailList= new ArrayList<>();
        String[] projections = {TCOL_3};
        Cursor cursor = db.query(TASK_TABLE,projections,null, null, null, null, null);
       /* if (cursor.getCount() < 1)
        {
            cursor.close();
            return "NOT EXIST";
        }*/
        if(cursor.moveToFirst()) {
            do {
                int idx = cursor.getColumnIndex(TCOL_3);
                emailList.add(cursor.getString(idx));
            } while (cursor.moveToNext());
        }else{
            return null;
        }
        Log.d(TAG, "Task List"+emailList);
        cursor.close();
        return emailList;
    }
    public ArrayList<String> retrieveFname(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> fnameList= new ArrayList<>();
        String[] projections = {COL_3};
        Cursor cursor = db.query(PERSON_TABLE,projections,null, null, null, null, null);
       /* if (cursor.getCount() < 1)
        {
            cursor.close();
            return "NOT EXIST";
        }*/
        if(cursor.moveToFirst()) {
            do {
                int idx = cursor.getColumnIndex(COL_3);
                fnameList.add(cursor.getString(idx));
            } while (cursor.moveToNext());
        }else{
            return null;
        }
        Log.d(TAG, "Task List"+fnameList);
        cursor.close();
        return fnameList;
    }

    public ArrayList<String> getEmails(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> emails= new ArrayList<>();
        String[] projections = {COL_5};
        Cursor cursor = db.query(PERSON_TABLE, projections,null, null, null, null, null);
        // String emails= cursor.getString(1);
        if(cursor.moveToFirst()) {
            do {
                int idx = cursor.getColumnIndex(COL_5);
                emails.add(cursor.getString(idx));
            } while (cursor.moveToNext());
        }else{
            return null;
        }
        cursor.close();
        return emails;

    }

   // ############################################################
  /*  public ArrayList<String> getFname(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> fname= new ArrayList<>();
        String[] projections = { SCOL_4, SCOL_5, COL_3};
        String whereClause=COL_1+"="+SCOL_2;
       String query=  "SELECT t1.firstname, t2.frmtime ,t2.totime FROM "+ PERSON_TABLE +"t1"+"INNER JOIN"+ SLOT_TABLE+ "t2 "+"ON"+ "t1.emailid = t2.emailid";
        Cursor cursor = db.query(true, PERSON_TABLE +" INNER JOIN "+ SLOT_TABLE, projections, whereClause,null, null, null, null, null);
        // String emails= cursor.getString(1);
        if(cursor.moveToFirst()) {
            do {
                int idx = cursor.getColumnIndex(COL_3);
                fname.add(cursor.getString(idx));
            } while (cursor.moveToNext());
        }else{
            return null;
        }
        Log.d(TAG, "Task List **************************"+fname);
        cursor.close();
        return fname;
    }*/
    //######################################################################

    public void deleteTask(String task){
        SQLiteDatabase db = this.getWritableDatabase();
        String[] projections = {TCOL_2};
        db.delete(TASK_TABLE,
                TCOL_2 + " = ?",
                new String[]{task});
        db.close();
    }
    public Cursor getInformation() {
        SQLiteDatabase db = this.getWritableDatabase();
        // String[] projections = {"firstname","emailid"};
        Cursor cur;
        //cur = db.query(TABLE_NAME, new String[]{"firstname","emailid"},null,null,null,null,null,null);
        cur=db.rawQuery("select * from "+PERSON_TABLE,null);
        // cur= db.rawQuery("select * from" + TABLE_NAME,null);
        return  cur;

        //return email;

    }
    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        // String[] projections = {"firstname","emailid"};
        Cursor cur;
        //cur = db.query(TABLE_NAME, new String[]{"firstname","emailid"},null,null,null,null,null,null);
        cur=db.rawQuery("select * from "+SLOT_TABLE,null);
        // cur= db.rawQuery("select * from" + TABLE_NAME,null);
        return  cur;

        //return email;

    }
    private String getDateTime(Date mdate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return dateFormat.format(mdate);
    }
    public ArrayList<String> retrieveSlotEmails(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> emailList= new ArrayList<>();
        String[] projections = {SCOL_2};
        Cursor cursor = db.query(SLOT_TABLE, projections, null, null, null, null, null);
        if(cursor.moveToFirst()) {
            do {
                int idx = cursor.getColumnIndex(SCOL_2);
                emailList.add(cursor.getString(idx));
            } while (cursor.moveToNext());
        }else{
            return null;
        }
        Log.d(TAG, "Task List **************************"+emailList);
        cursor.close();
        return emailList;
    }

    public ArrayList<String> retrieveSlotFrom(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> fromList= new ArrayList<>();
        String[] projections = {SCOL_4};
        Cursor cursor = db.query(SLOT_TABLE,projections,null, null, null, null, null);
        if(cursor.moveToFirst()) {
            do {
                int idx = cursor.getColumnIndex(SCOL_4);
                fromList.add(cursor.getString(idx));
            } while (cursor.moveToNext());
        }else{
            return null;
        }
        Log.d(TAG, "Task List **************************"+fromList);
        cursor.close();
        return fromList;
    }
    public ArrayList<String> retrieveSlotTo(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> toList= new ArrayList<>();
        String[] projections = {SCOL_5};
        Cursor cursor = db.query(SLOT_TABLE,projections,null, null, null, null, null);
        if(cursor.moveToFirst()) {
            do {
                int idx = cursor.getColumnIndex(SCOL_5);
                toList.add(cursor.getString(idx));
            } while (cursor.moveToNext());
        }else{
            return null;
        }
        Log.d(TAG, "Task List **************************"+toList);
        cursor.close();
        return toList;
    }


}
