package com.example.harita.pickslot;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText emailData, password;
    Button loginButton;
    DataBaseHelper persondb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        loginButton = (Button) this.findViewById(R.id.login_b);
        assert loginButton != null;

        persondb = new DataBaseHelper(this);
        verifyData();

    }

    private void verifyData() {
        final AlertDialog alrt = new AlertDialog.Builder(this).create();
        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                emailData = (EditText) findViewById(R.id.email_address);
                String email = emailData.getText().toString();
                password = (EditText) findViewById(R.id.password);
                String pswd = password.getText().toString();
                //emailData.getText().equals(null)
                alrt.setTitle("Missing Value");
                alrt.setMessage("Please Enter all the values");
                if (email.isEmpty() || pswd.isEmpty()) {
                    alrt.show();

                } else {
                    String storedPassword = persondb.getSinlgeEntry(email);
                    if (pswd.equals(storedPassword)) {
                        Toast.makeText(LoginActivity.this, "Congrats: Login Successfull", Toast.LENGTH_LONG).show();
                        //Toast.makeText(LoginActivity.this, "Data Successfully Inserted", Toast.LENGTH_LONG).show();
                       // startActivity(main);
                    } else {
                        Toast.makeText(LoginActivity.this, "User Name or Password does not match", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
}
