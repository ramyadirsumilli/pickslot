package com.example.harita.pickslot;

/**
 * Created by harita on 25/05/2016.
 */
public class DataRetrive {
    private String name;
    private String email;
    private int pos;
    public  DataRetrive()
    {}
    public DataRetrive(String name,String email,int pos)
    {
        this.email=email;
        this.name=name;
        this.pos=pos;

    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

}
