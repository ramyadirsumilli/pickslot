package com.example.harita.pickslot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class UserSettings extends AppCompatActivity {

    Button pinButton, changePswdButton, taskButton, addSlotButton, delSlotButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);
        pinButton = (Button) this.findViewById(R.id.pin_b);
        assert pinButton != null;
        pinButton.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent log = new Intent(UserSettings.this,pin.class);
                startActivity(log);
            }

        }
        ));
        changePswdButton = (Button) this.findViewById(R.id.ch_pswd_b);
        assert changePswdButton != null;
        changePswdButton.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent log = new Intent(UserSettings.this,changePswd.class);
                startActivity(log);
            }

        }
        ));
        taskButton = (Button) this.findViewById(R.id.task_b);
        assert taskButton != null;
        taskButton.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent log = new Intent(UserSettings.this,TaskActivity.class);
                startActivity(log);
            }

        }
        ));
        addSlotButton = (Button) this.findViewById(R.id.add_slot_b);
        assert addSlotButton != null;
        addSlotButton.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent log = new Intent(UserSettings.this,addSlot.class);
                startActivity(log);
            }

        }
        ));
        /*
        delSlotButton = (Button) this.findViewById(R.id.delete_slot_b);
        assert delSlotButton != null;
        delSlotButton.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent log = new Intent(UserSettings.this,delSlot.class);
                startActivity(log);
            }

        }
        ));*/
    }
}
