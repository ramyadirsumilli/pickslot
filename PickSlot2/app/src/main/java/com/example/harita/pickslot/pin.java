package com.example.harita.pickslot;

import android.app.AlertDialog;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class pin extends AppCompatActivity {

    EditText emailData, password;
    Button ConfirmPin_b;
    DataBaseHelper persondb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);
        ConfirmPin_b = (Button) this.findViewById(R.id.pinGenerate_b);
        assert  ConfirmPin_b!=null;
        persondb = new DataBaseHelper(this);
        enterPin();


    }
    private void enterPin() {
        final AlertDialog alrt = new AlertDialog.Builder(this).create();
        final AlertDialog alrt1 = new AlertDialog.Builder(this).create();
        ConfirmPin_b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                emailData = (EditText) findViewById(R.id.email_address);
                String email = emailData.getText().toString();
                password = (EditText) findViewById(R.id.password);
                String pswd = password.getText().toString();

                String storedPassword = persondb.getSinlgeEntry(email);
                if (pswd.equals(storedPassword)) {
                    String pin = ""+((int)(Math.random()*9000)+1000);
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("plain/text");
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
                    intent.putExtra(Intent.EXTRA_SUBJECT, "PickSlot Auto-Generated Pin");
                    intent.putExtra(Intent.EXTRA_TEXT, "your PickSLot easy access pin is " +pin);
                  //  intent.setAction(Intent.ACTION_SENDTO);
                    startActivity(Intent.createChooser(intent, "Send Email"));
                    Toast.makeText(pin.this, "Pin Successfully Generated", Toast.LENGTH_LONG).show();
                    boolean p_update = persondb.updatePin(email,pin);
                    if(p_update= true) Toast.makeText(pin.this, "Congrats: Pin Successfully Updated", Toast.LENGTH_LONG).show();
                    else Toast.makeText(pin.this, "Failed to Change Pin", Toast.LENGTH_LONG).show();
                // startActivity(main);

            } else {
                Toast.makeText(pin.this, "User Name or Password does not match", Toast.LENGTH_LONG).show();
            }
            }
        });
    }
}
