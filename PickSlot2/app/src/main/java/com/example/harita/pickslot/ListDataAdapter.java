package com.example.harita.pickslot;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harita on 25/05/2016.
 */
public class ListDataAdapter extends ArrayAdapter {
    private static final String TAG = "TaskActivity";

    List<Object> list = new ArrayList<Object>();




    public ListDataAdapter(Context context, int resource) {
        super(context, resource);
    }

    public ListDataAdapter(Context context, int resource, List<Object> list) {
        super(context, resource);
        this.list = list;
    }

    static class LayoutHandler
    {
        TextView Name;
        TextView Email;
        ImageButton delteBut;
    }

    @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row=convertView;

        LayoutHandler layoutHandler;
        if(row==null)
        {
            LayoutInflater layoutInflater=(LayoutInflater)(this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE));
            row=layoutInflater.inflate(R.layout.rowlayout,parent,false);
            layoutHandler=new LayoutHandler();
            layoutHandler.Name=(TextView)row.findViewById(R.id.Fname);
            layoutHandler.Email=(TextView)row.findViewById(R.id.email);
            layoutHandler.delteBut = (ImageButton) row.findViewById(R.id.btnDelete);
            if (position%2== 0) {
                row.setBackgroundResource(R.color.colorlist);
            } else if (position%2 == 1) {
                row.setBackgroundResource(R.color.colorAccent);
            }
            row.setTag(layoutHandler);
        }
        else layoutHandler = (LayoutHandler) row.getTag();

        final DataRetrive dataRetrive=(DataRetrive) this.getItem(position);
        layoutHandler.Name.setText(dataRetrive.getName());
        layoutHandler.Email.setText(dataRetrive.getEmail());
        final View finalRow = row;
        layoutHandler.delteBut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                int pst=dataRetrive.getPos();
                list.remove(pst);
                //}
                notifyDataSetChanged();

            }
        });
        return row;
        //return super.getView(position, convertView, parent);
    }
}
